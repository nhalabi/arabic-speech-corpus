import glob



def getPhones(text):
	lines = text.splitlines()
	
	phones = ''
	
	for line in lines:
		if line.strip() == 'item [2]:':
			break
			
		sline = line.strip()
		
		parts = sline.split('=')
		
		if len(parts) > 1 and parts[0].strip() == 'text':
			phones += parts[1].strip().strip('"') + ' '
			
	return phones.strip()

res = u''
for file in glob.glob('textgrid/*'):
	f = open(file, 'r')
	
	number = file.split(' ')[-1].split('.')[0]
	
	phones = getPhones(f.read())
	
	res +=  '"ARA NORM  ' + number + '.wav" "' + phones + '"' + '\n'
	
	f.close()

out = open('phonetic-transcript.txt', 'w')
out.write(res)
out.close()